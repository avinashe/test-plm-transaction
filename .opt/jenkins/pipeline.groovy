env.mvnCfg = "cfg/maven/CI/settings.xml"
env.mvnHome = '/opt/amdocs/external/maven-3.3.9'
env.javaHome = '/usr/java/jdk1.8.0_102'
env.mvnParams = "-B -s ${mvnCfg} -Ddocker.image.release=${env.BUILD_NUMBER}"
env.bitbucketNotification = true
env.sonarEnv = 'Sonar_PartnerPlace'
env.pushToK8 = true
env.KUBECONFIG = '/users/kube/.kube/config'
env.sonarHostUrl = 'http://illin5566:19090'
env.sonarProjectName = "partnerplace-incubation-ms"
env.sonarAuth = "c9f6e35203db21f077e508225ba9f44267d66194:"
env.sonarProjectKey = 'com.amdocs.partnerplace:partnerplace-incubation-ms'
env.passParams = '-e WARN -e OK'
env.sonarSleepInterval = 10
env.couchbaseHost = '100.100.100.100'
env.emailAddressesMasterRelease = 'franklinl@amdocs.com'
env.cxExcludeFolders = 'src/test'
env.cxGroupId = '7a6415dc-4530-42f1-a46d-3b998b22f4e1'
env.cxServerUrl = 'http://cxservername'
env.cxUsername = 'CXUser'
env.pactBroker = 'http://pactmachine:55555'
env.dockerRegistry = 'illin5225.corp.amdocs.com:5000'