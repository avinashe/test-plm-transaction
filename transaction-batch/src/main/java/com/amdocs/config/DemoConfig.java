package com.amdocs.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.support.ListItemReader;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * The main configuration class.
 */
@Configuration
@EnableTask
@EnableBatchProcessing
@EnableConfigurationProperties({DemoBatchTaskProperties.class})
@Slf4j
public class DemoConfig {

    private static final int DEFAULT_CHUNK_COUNT = 3;

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    private DemoBatchTaskProperties properties;

    public DemoConfig() {
    }

    @Bean
    public Job job1() {
        return this.jobBuilderFactory
                .get("job1")
                .start(this.stepBuilderFactory
                        .get("job1step1").tasklet(new Tasklet() {
                            public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                                DateFormat dateFormat = new SimpleDateFormat(DemoConfig.this.properties.getFormat());
                                log.info(String.format("Job1 was run with date %s", dateFormat.format(new Date())));
                                return RepeatStatus.FINISHED;
                            }
                        }).build())
                .build();
    }

    @Bean
    public Job job2() {
        return this.jobBuilderFactory
                .get("job2")
                .start(this.stepBuilderFactory
                        .get("job2step1")
                        .tasklet(new Tasklet() {
                            public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                                DateFormat dateFormat = new SimpleDateFormat(DemoConfig.this.properties.getFormat());
                                log.info(String.format("Job2 was run with date %s", dateFormat.format(new Date())));
                                return RepeatStatus.FINISHED;
                            }
                        }).build())
                .build();
    }

    @Bean
    public Step step1() {
        return this.stepBuilderFactory.get("step1")
                .tasklet(new Tasklet() {
                    @Override
                    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
                        log.info("Tasklet has run");
                        return RepeatStatus.FINISHED;
                    }
                }).build();
    }

    @Bean
    public Step step2() {
        return this.stepBuilderFactory.get("step2")
                .<String, String>chunk(DEFAULT_CHUNK_COUNT)
                .reader(new ListItemReader<>(Arrays.asList("1", "2", "3", "4", "5", "6")))
                .processor(new ItemProcessor<String, String>() {
                    @Override
                    public String process(String item) throws Exception {
                        return String.valueOf(Integer.parseInt(item) * -1);
                    }
                })
                .writer(new ItemWriter<String>() {
                    @Override
                    public void write(List<? extends String> items) throws Exception {
                        for (String item : items) {
                            System.out.println(">> " + item);
                        }
                    }
                }).build();
    }

    @Bean
    public Job job() {
        return this.jobBuilderFactory.get("job3")
                .start(step1())
                .next(step2())
                .build();
    }
}
