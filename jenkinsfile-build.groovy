pipeline {
    agent any
    environment {
        subdir = 'test-plm-transaction'
        name = 'transaction-batch-2-task'
        dockerRegistry = 'uxp-hub-docker.corp.amdocs.com'
    }
    parameters {
        string (
                name: 'branchName',
                defaultValue: 'master',
                description: "Branch to build/test.")
        string(
                name:'buildInfo',
                defaultValue: '-0.0.0'
        )
        booleanParam(name: 'skipUnitTests', defaultValue: false)
    }

    stages{
        stage('checkout'){
            when {
                branch 'master'
            }
            steps{
                echo 'checkout or clone branch'
            }
        }
        stage('test') {
            when {
                branch 'master'
            }
            steps {
                mvnTest()
            }
        }
        stage('Maven Deploy & Image Build'){
            when {
                branch 'master'
            }
            steps {
                script{
                    mavenWithGoals("deploy -DbuildInfo=${params.buildInfo} -DskipTests" +
                            " -Pwith-image -f pom.xml -Ddocker.push.registry=${env.dockerRegistry}")
              }
            }
        }
    }
    post {
        always {
            echo 'Build finished'
        }
        success {
            echo 'Successful build.'
        }
        unstable {
            echo 'Failed tests'
        }
        failure {
            echo 'Failed to build'
        }
    }
}

def mvnTest(){
    sh "mvn clean verify"
}

def mavenWithGoals(goals, boolean archiveArtifacts = false, String mavenSettingsConfig = 'cecedd343002696d0abb50b32b541b8a6ba2883f') {

    build_var = sh(returnStdout: true, script: "echo $goals | sed 's/.*-DbuildImage=//g' | sed 's/ .*//g'").trim()
    int status = sh(script: "echo $goals | grep buildImage", returnStatus: true)
    sh "echo $build_var"

    sh "echo Using provided Maven Settings: ${mavenSettingsConfig}"

        if( status != 0 ) {
            sh "mvn $goals"
            echo "status is ------------------${status}"
        } else{
            echo "status is ------------------${status}"
            sh "mvn $goals -DbuildImage=\"$build_var\" -Dbuild-image=\"$build_var\""
        }
}