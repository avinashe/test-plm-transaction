package com.amdocs.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    @Autowired
    private DemoBatchTaskProperties demoBatchTaskProperties;

    @Bean
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSourceProperties dataSourceProperties() {
        return new DataSourceProperties();
    }

    @Bean(name = "dataSource")
    @Primary
    public DataSource dataSource() {
        return dataSourceProperties().initializeDataSourceBuilder().build();
    }

    @Bean(name = "userDataSource")
    public DataSource userServiceDataSource() {
        return demoBatchTaskProperties.getBusinessDatasource().initializeDataSourceBuilder().build();
    }

    @Bean(name = "jdbcUserService")
    @Autowired
    public JdbcTemplate userServiceJdbcTemplate(@Qualifier("userDataSource") DataSource userServiceDs) {
        return new JdbcTemplate(userServiceDs);
    }
}
