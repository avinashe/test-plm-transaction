package com.amdocs.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("demo")
@Data
public class DemoBatchTaskProperties {
    /**
     * The timestamp format, "yyyy-MM-dd HH:mm:ss.SSS" by default.
     */
    private String format = "yyyy-MM-dd HH:mm:ss.SSS";

    /**
     * The business datasource configuration.
     */
    private DataSourceProperties businessDatasource = new DataSourceProperties();
}
